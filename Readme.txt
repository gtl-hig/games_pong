
sfml_01
Original example of Pong from SFML 2.1



sfml_02 
Engineered and refactored code to showcase Object Oriented design. 
It showcases an architecture suitable for various game programming tasks,
with a simple world, physics and ai sub-components. 
The design allows building more complex games.



sfml_03 (work in progress)
This is the 2 player networked version of sfml_02 example. Some additional
refactoring and engineering to make the networking sub-system work.






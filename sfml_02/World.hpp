//
//  World.h
//  pong_sfml
//
//  Created by Mariusz Nowostawski on 8/11/13.
//  Copyright (c) 2013 Mariusz Nowostawski. All rights reserved.
//

#ifndef __pong_sfml__World__
#define __pong_sfml__World__

#include "Ball.hpp"
#include "Paddle.hpp"




class World
{
    
public:
    
    World(){}
    World(float width, float height);
    ~World();
    
#pragma mark

    void resetWorld();
    
    Paddle* getPlayerPaddle();
    
    Paddle* getLeftPaddle();
    Paddle* getRightPaddle();
    
    Ball* getBall();

    float getWidth() { return gameWidth; }
    float getHeight() { return gameHeight; }
    
    
    
private:
#pragma mark

    
    void createBall();
    void createPaddles();

    
// models
    
    Paddle* leftPaddle;
    Paddle* rightPaddle;
    Ball* ball;
    
    float gameWidth;
    float gameHeight;

    
// constants
    const sf::Vector2f PADDLE_DEFAULT_SIZE = sf::Vector2f(25, 100);
    const float PADDLE_DEFAULT_SPEED = 400.f;
    const float BALL_DEFAULT_RADIUS = 10.f;

    
};

#endif /* defined(__pong_sfml__World__) */

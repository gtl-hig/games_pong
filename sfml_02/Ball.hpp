//
//  Ball.h
//  pong_sfml
//
//  Created by Mariusz Nowostawski on 7/11/13.
//  Copyright (c) 2013 Mariusz Nowostawski. All rights reserved.
//

#ifndef __pong_sfml__Ball__
#define __pong_sfml__Ball__


#include <SFML/Graphics.hpp>

#include <cmath>


class Ball : public sf::CircleShape
{
    
public:
    
    Ball();
    virtual ~Ball();

    virtual void operator=(const Ball &ball);

    
    const float speed   = 400.f;
    float angle         = 0.f;
    float radius        = 0.f;
    
};


#endif /* defined(__pong_sfml__Ball__) */

//
//  main.m
//  pong_sfml
//
//  Created by Mariusz Nowostawski on 6/11/13.
//  Copyright (c) 2013 Mariusz Nowostawski. All rights reserved.
//

#import "Pong.hpp"

int main(int argc, const char * argv[])
{
    
    Pong pong;
    pong.run();

}

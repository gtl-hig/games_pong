//
//  Paddle.cpp
//  pong_sfml
//
//  Created by Mariusz Nowostawski on 7/11/13.
//  Copyright (c) 2013 Mariusz Nowostawski. All rights reserved.
//



#include <iostream>

#include "Paddle.hpp"



Paddle::Paddle()
{
    setOutlineThickness(3);
    setOutlineColor(sf::Color::Black);
    setFillColor(sf::Color(100, 100, 200));
}

Paddle::~Paddle() {}

void Paddle::operator=(const Paddle &paddle)
{
    setSize(paddle.getSize());
    setOutlineColor(paddle.getOutlineColor());
    setOutlineThickness(paddle.getOutlineThickness());
    setFillColor(paddle.getFillColor());
    setOrigin(paddle.getSize() / 2.f);
}
//
//  Ball.cpp
//  pong_sfml
//
//  Created by Mariusz Nowostawski on 7/11/13.
//  Copyright (c) 2013 Mariusz Nowostawski. All rights reserved.
//

#include "Ball.hpp"



Ball::Ball()
{
    setOutlineThickness(3);
    setOutlineColor(sf::Color::Black);
    setFillColor(sf::Color::White);
}


Ball::~Ball(){}


void Ball::operator=(const Ball &ball)
{
    setRadius(ball.getRadius());
    setOutlineThickness(ball.getOutlineThickness());
    setOutlineColor(ball.getOutlineColor());
    setFillColor(ball.getFillColor());
    setOrigin(ball.getOrigin());
}



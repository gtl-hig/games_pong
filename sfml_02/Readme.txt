
+ Intro: Pong game

    Pong has very simple world:

        - 2 paddles
        - 1 ball
        - edges of the screen (4 of them)

        In single player mode, it needs a simple AI bot to play one of the paddles.

        In two-player mode, it needs network support.

        The original code (sfml_01) is one long method call. It is difficult to think about the code, 
        and port it to a two-player networking game. So, here there is a more "refactored" version.



+ Object Oriented design

	sfml_02 is a skeleton of a game demonstrating basic program elements:

    - simple world with world elements (2 Paddles and a Ball)
    - main game loop
    - keyboard event processing
    - game events processing
    - world updates
    - simple bot updates
    - rendering

    This code is functionally identical to code from sfml_01. It is effectively the same code,
    that has been refactored and engineered for extensibility. 

    Using the code from sfml_01 think, what you would need to do if you wanted to provide 
    scoring, or two-player mode with networking? Would it be possible? 
    
    


+ Dependencies

	Everything is done in SFML 2.1 and C++. No additional dependencies. 



+ Stats

	7 classes
	1000 lines of code (.cpp + .hpp)

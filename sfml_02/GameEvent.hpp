//
//  GameEvent.h
//  pong_sfml
//
//  Created by Mariusz Nowostawski on 8/11/13.
//  Copyright (c) 2013 Mariusz Nowostawski. All rights reserved.
//

#ifndef __pong_sfml__GameEvent__
#define __pong_sfml__GameEvent__


static const int GAME_END_LEFT_WINS = 1;
static const int GAME_END_RIGHT_WINS = 2;



class GameEvent
{
    
public:
    
    int id;
    void* data;
};


#endif /* defined(__pong_sfml__GameEvent__) */

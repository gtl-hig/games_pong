//
//  World.cpp
//  pong_sfml
//
//  Created by Mariusz Nowostawski on 8/11/13.
//  Copyright (c) 2013 Mariusz Nowostawski. All rights reserved.
//

#include <iostream>

#include "World.hpp"



World::World(float width, float height)
{
    gameWidth = width;
    gameHeight = height;
    createBall();
    createPaddles();
}

World::~World()
{
    delete ball;
    delete leftPaddle;
    delete rightPaddle;
}



#pragma mark

void World::createBall()
{
    ball = new Ball();
    ball->setRadius(BALL_DEFAULT_RADIUS - 3);
    ball->setOrigin(BALL_DEFAULT_RADIUS / 2, BALL_DEFAULT_RADIUS / 2);
}


void World::createPaddles()
{
    leftPaddle = new Paddle();
    leftPaddle->setOrigin(PADDLE_DEFAULT_SIZE / 2.f);
    leftPaddle->setSize(PADDLE_DEFAULT_SIZE - sf::Vector2f(3, 3));
    leftPaddle->speed = PADDLE_DEFAULT_SPEED;
    
    rightPaddle = new Paddle();
    rightPaddle->setOrigin(PADDLE_DEFAULT_SIZE / 2.f);
    rightPaddle->setSize(PADDLE_DEFAULT_SIZE - sf::Vector2f(3, 3));
}

void World::resetWorld()
{
    leftPaddle->setPosition(20, gameHeight / 2);
    rightPaddle->setPosition(gameWidth - 20, gameHeight / 2);
    ball->setPosition(gameWidth / 2, gameHeight / 2);
}

Paddle* World::getPlayerPaddle()
{
    return leftPaddle;
}

#pragma mark

Paddle* World::getLeftPaddle()
{
    return leftPaddle;
}

Paddle* World::getRightPaddle()
{
    return rightPaddle;
}

Ball* World::getBall()
{
    return ball;
}




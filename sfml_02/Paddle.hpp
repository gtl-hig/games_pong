//
//  Paddle.h
//  pong_sfml
//
//  Created by Mariusz Nowostawski on 7/11/13.
//  Copyright (c) 2013 Mariusz Nowostawski. All rights reserved.
//

#ifndef __pong_sfml__Paddle__
#define __pong_sfml__Paddle__


#include <SFML/Graphics.hpp>



class Paddle : public sf::RectangleShape
{
    
public:
    
    Paddle();
    virtual ~Paddle();
    
    virtual void operator=(const Paddle& paddle);
    
    
    
    float speed  = 0.f;
    
    
};



#endif /* defined(__pong_sfml__Paddle__) */

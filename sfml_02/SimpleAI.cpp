//
//  SimpleAI.cpp
//  pong_sfml
//
//  Created by Mariusz Nowostawski on 8/11/13.
//  Copyright (c) 2013 Mariusz Nowostawski. All rights reserved.
//

#include "SimpleAI.hpp"



void SimpleAI::updateBot(float deltaTime)
{
    
    // Move the computer's paddle
    Paddle* rightPaddle = world->getRightPaddle();
    if (((rightPaddle->speed < 0.f) && (rightPaddle->getPosition().y - rightPaddle->getSize().y / 2 > 5.f)) ||
        ((rightPaddle->speed > 0.f) && (rightPaddle->getPosition().y + rightPaddle->getSize().y / 2 < gameHeight - 5.f)))
    {
        rightPaddle->move(0.f, rightPaddle->speed * deltaTime);
    }
    
    // Update the computer's paddle direction according to the ball position
    Ball* ball = world->getBall();
    if (AITimer.getElapsedTime() > AITime)
    {
        AITimer.restart();
        if (ball->getPosition().y + ball->getRadius() > rightPaddle->getPosition().y + rightPaddle->getSize().y / 2)
            rightPaddle->speed = 400.f;
        else if (ball->getPosition().y - ball->getRadius() < rightPaddle->getPosition().y - rightPaddle->getSize().y / 2)
            rightPaddle->speed = -400.f;
        else
            rightPaddle->speed = 0.f;
    }
    
}
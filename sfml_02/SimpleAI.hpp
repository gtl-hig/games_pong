//
//  SimpleAI.h
//  pong_sfml
//
//  Created by Mariusz Nowostawski on 8/11/13.
//  Copyright (c) 2013 Mariusz Nowostawski. All rights reserved.
//

#ifndef __pong_sfml__SimpleAI__
#define __pong_sfml__SimpleAI__


#include "World.hpp"



class SimpleAI
{
    
public:
    
    SimpleAI(){}
    SimpleAI(World* world) : world(world), gameWidth(world->getWidth()), gameHeight(world->getHeight()) {}
    
    void updateBot(float deltaTime);
    
    
    
private:
    
    World* world;
    
    sf::Clock AITimer;
    const sf::Time AITime = sf::seconds(0.1f);

    float gameWidth;
    float gameHeight;
};

#endif /* defined(__pong_sfml__SimpleAI__) */

//
//  SimplePhysics.cpp
//  pong_sfml
//
//  Created by Mariusz Nowostawski on 8/11/13.
//  Copyright (c) 2013 Mariusz Nowostawski. All rights reserved.
//

#include "SimplePhysics.hpp"

#include "GameEvent.hpp"





SimplePhysics::SimplePhysics(World* world)
    : world(world), gameWidth(world->getWidth()), gameHeight(world->getHeight())
{
    // Setup Sound subsystem for ball sounds
    sf::SoundBuffer ballSoundBuffer;
    if (!ballSoundBuffer.loadFromFile("resources/ball.wav"))
    {
        std::cout << "Cannot find ball sound file, must quit.\n";
    }
    ballSound = sf::Sound(ballSoundBuffer);
}



#pragma mark


void SimplePhysics::updateBallState(float deltaTime)
{
    Ball* ball = world->getBall();
    float factor = ball->speed * deltaTime;
    ball->move(std::cos(ball->angle) * factor, std::sin(ball->angle) * factor);
}


void SimplePhysics::resetBallAngle()
{
    Ball* ball = world->getBall();
    do
    {
        // Make sure the ball initial angle is not too much vertical
        ball->angle = (std::rand() % 360) * 2 * PI / 360;
    }
    while (std::abs(std::cos(ball->angle)) < 0.7f);
}



//////////////////////////////////////////////////////////////////////
///  Collisions
//////////////////////////////////////////////////////////////////////

void SimplePhysics::checkCollisions(std::queue<GameEvent*>* eventQueue)
{
    checkBallScreenCollision(eventQueue);
    checkBallPaddleCollisions();
    
}



//////////////////////////////////////////////////////////////////////
///  Player movements
//////////////////////////////////////////////////////////////////////

void SimplePhysics::movePlayerUp(float deltaTime)
{
    Paddle* p = world->getPlayerPaddle();
    if (p->getPosition().y - p->getSize().y / 2 > 5.f)
    {
        p->move(0.f, -p->speed * deltaTime);
    }
}

void SimplePhysics::movePlayerDown(float deltaTime)
{
    Paddle* p = world->getPlayerPaddle();
    if (p->getPosition().y + p->getSize().y / 2 < gameHeight - 5.f)
    {
        p->move(0.f, p->speed * deltaTime);
    }
}



#pragma mark


//////////////////////////////////////////////////////////////////////
///  Collisions with screen edges and paddles checks
//////////////////////////////////////////////////////////////////////


void SimplePhysics::checkBallScreenCollision(std::queue<GameEvent*>* eventQueue)
{
    Ball* ball = world->getBall();
    if (isBallScreenCollisionLeft(ball))
    {
        GameEvent* e = new GameEvent();
        e->id = GAME_END_RIGHT_WINS;
        eventQueue->push(e);
    }
    
    if (isBallScreenCollisionRight(ball))
    {
        GameEvent* e = new GameEvent();
        e->id = GAME_END_LEFT_WINS;
        eventQueue->push(e);
    }
    
    if (isBallScreenCollisionBottom(ball))
    {
        ballSound.play();
        ball->angle = -ball->angle;
        ball->setPosition(ball->getPosition().x, ball->getRadius() + 0.1f);
    }
    
    if (isBallScreenCollisionTop(ball))
    {
        ballSound.play();
        ball->angle = -ball->angle;
        ball->setPosition(ball->getPosition().x, gameHeight - ball->getRadius() - 0.1f);
    }
    
}


void SimplePhysics::checkBallPaddleCollisions()
{
    Ball* ball = world->getBall();
    Paddle* leftPaddle = world->getLeftPaddle();
    Paddle* rightPaddle = world->getRightPaddle();
    
    if (isBallPaddleCollision(ball, leftPaddle))
    {
        if (ball->getPosition().y > leftPaddle->getPosition().y)
            ball->angle = PI - ball->angle + (std::rand() % 20) * PI / 180;
        else
            ball->angle = PI - ball->angle - (std::rand() % 20) * PI / 180;
        
        ballSound.play();
        ball->setPosition(leftPaddle->getPosition().x + ball->getRadius() + leftPaddle->getSize().x / 2 + 0.1f,
                          ball->getPosition().y);
    }
    
    if (isBallPaddleCollision(ball, rightPaddle))
    {
        if (ball->getPosition().y > rightPaddle->getPosition().y)
            ball->angle = PI - ball->angle + (std::rand() % 20) * PI / 180;
        else
            ball->angle = PI - ball->angle - (std::rand() % 20) * PI / 180;
        
        ballSound.play();
        ball->setPosition(rightPaddle->getPosition().x - ball->getRadius() - rightPaddle->getSize().x / 2 - 0.1f,
                          ball->getPosition().y);
    }
    
}


bool SimplePhysics::isBallScreenCollisionLeft(const Ball *ball)
{
    return (ball->getPosition().x - ball->getRadius() < 0.f);
}



bool SimplePhysics::isBallScreenCollisionRight(const Ball *ball)
{
    return (ball->getPosition().x + ball->getRadius() > gameWidth);
}


bool SimplePhysics::isBallScreenCollisionBottom(const Ball *ball)
{
    return (ball->getPosition().y - ball->getRadius() < 0.f);
}


bool SimplePhysics::isBallScreenCollisionTop(const Ball *ball)
{
    return (ball->getPosition().y + ball->getRadius() > gameHeight);
}


bool SimplePhysics::isBallPaddleCollision(const Ball *ball, const Paddle *paddle)
{
    const float ball_r = ball->getRadius();
    const float ball_x = ball->getPosition().x;
    const float ball_y = ball->getPosition().y;
    
    const float ball_left_edge = ball_x - ball_r;
    const float ball_right_edge = ball_x + ball_r;
    const float ball_bottom_edge = ball_y - ball_r;
    const float ball_top_edge = ball_y + ball_r;
    
    const float paddle_offset_x = paddle->getSize().x / 2;
    const float paddle_offset_y = paddle->getSize().y / 2;
    const float paddle_right_edge = paddle->getPosition().x + paddle_offset_x;
    const float paddle_left_edge = paddle->getPosition().x - paddle_offset_x;
    const float paddle_top_edge = paddle->getPosition().y + paddle_offset_y;
    const float paddle_bottom_edge = paddle->getPosition().y - paddle_offset_y;
    
    return
        // collision on the left side check
        (ball_left_edge < paddle_right_edge &&
         ball_left_edge > paddle->getPosition().x &&
         ball_top_edge >= paddle_bottom_edge &&
         ball_bottom_edge <= paddle_top_edge)
    
    ||
        // collision on the right side check
        (ball_right_edge > paddle_left_edge &&
         ball_right_edge < paddle->getPosition().x &&
         ball_top_edge >= paddle_bottom_edge &&
         ball_bottom_edge <= paddle_top_edge);
}






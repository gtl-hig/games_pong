//
//  SimplePhysics.h
//  pong_sfml
//
//  Created by Mariusz Nowostawski on 8/11/13.
//  Copyright (c) 2013 Mariusz Nowostawski. All rights reserved.
//

#ifndef __pong_sfml__SimplePhysics__
#define __pong_sfml__SimplePhysics__

#include <iostream>
#include <queue>

#include <SFML/Audio.hpp>

#include "Ball.hpp"
#include "Paddle.hpp"
#include "World.hpp"
#include "GameEvent.hpp"


static const float PI = 3.14159f;


class SimplePhysics
{
    
public:
    
    SimplePhysics() : world(nullptr), gameWidth(0), gameHeight(0) {}
    SimplePhysics(World* world);
    
    
    void resetBallAngle();
    void updateBallState(float deltaTime);
    
    void checkCollisions(std::queue<GameEvent*>* eventQueue);
    
    void movePlayerUp(float deltaTime);
    void movePlayerDown(float deltaTime);
    
    
private:
    
    void checkBallScreenCollision(std::queue<GameEvent*>* eventQueue);
    void checkBallPaddleCollisions();

    bool isBallScreenCollisionLeft(const Ball *ball);
    bool isBallScreenCollisionRight(const Ball *ball);
    bool isBallScreenCollisionBottom(const Ball *ball);
    bool isBallScreenCollisionTop(const Ball *ball);
    
    bool isBallPaddleCollision(const Ball *ball, const Paddle *paddle);
    
    
    World* world;
    float gameWidth;
    float gameHeight;
    
    sf::Sound ballSound;
    
    
};

#endif /* defined(__pong_sfml__SimplePhysics__) */

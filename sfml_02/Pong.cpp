
////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////

#include <string>

#include "Pong.hpp"





////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

Pong::Pong(void)
{
    std::srand(static_cast<unsigned int>(std::time(NULL)));
    window = new sf::RenderWindow(sf::VideoMode(gameWidth, gameHeight, 32), "SFML Pong");
    window->setVerticalSyncEnabled(true);
    window->clear();
    
    if (!font.loadFromFile("resources/sansation.ttf"))
        handleError("Cannot find fonts, must quit.\n");
    
    world = new World(gameWidth, gameHeight);
    physics = SimplePhysics(world);
    ai = new SimpleAI(world);
    
    eventQueue = new std::queue<GameEvent*>();
}

Pong::~Pong(void)
{
    delete window;
    delete eventQueue;
    delete ai;
}




#pragma mark



///////////////////////////////////////////////////////////////////////////////////
/// MAIN GAME LOOP
///////////////////////////////////////////////////////////////////////////////////

void Pong::run(void)
{
    initializePauseMessage();
    
    while (window->isOpen())
    {
        handleControllerEvents();
        if (isPlaying) {
            processGameEvents();
            updateWorld();
        }
        renderWorld();
    }
    
}




#pragma mark



void Pong::restartGame()
{
    clock.restart();
    // Reset the position of the paddles and the ball
    world->resetWorld();
    physics.resetBallAngle();
    // clean up the game events queue
    std::queue<GameEvent*> empty;
    std::swap(*eventQueue, empty);
    // let's go
    isPlaying = true;
}



void Pong::handleControllerEvents()
{
    sf::Event event;
    while (window->pollEvent(event))
    {
        // Window closed or escape key pressed: exit
        if ((event.type == sf::Event::Closed) ||
            ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape)))
        {
            window->close();
            break;
        }
        
        // Space key pressed: play
        if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Space))
        {
            if (!isPlaying) restartGame();
        }
    }
    
    if (isPlaying)
    {
        deltaTime = clock.restart().asSeconds();
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            physics.movePlayerUp(deltaTime);
        }
        
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            physics.movePlayerDown(deltaTime);
        }
    }
}



void Pong::processGameEvents()
{
    GameEvent* e;
    while ( eventQueue->size() > 0 ) {
        switch (e->id)
        {
            case GAME_END_RIGHT_WINS:
                isPlaying = false;
                pauseMessage.setString("You lost !\nPress space to restart or\nescape to exit");
                break;
            case GAME_END_LEFT_WINS:
                isPlaying = false;
                pauseMessage.setString("You won !\nPress space to restart or\nescape to exit");
                break;
        }
        eventQueue->pop();
    }
}



void Pong::renderWorld()
{
    window->clear(sf::Color(220, 220, 220));
    if (isPlaying)
    {
        // Draw the paddles and the ball
        window->draw(*(world->getLeftPaddle()));
        window->draw(*(world->getRightPaddle()));
        window->draw(*(world->getBall()));
    } else {
        // Draw the pause message
        window->draw(pauseMessage);
    }
    
    // Display all things on screen
    window->display();
}



void Pong::updateWorld()
{
    physics.updateBallState(deltaTime);
    physics.checkCollisions(eventQueue);
    
    ai->updateBot(deltaTime);
}


//////
/// Sets up first welcome screen
//////
void Pong::initializePauseMessage(void)
{
    pauseMessage.setFont(font);
    pauseMessage.setCharacterSize(40);
    pauseMessage.setPosition(170.f, 150.f);
    pauseMessage.setColor(sf::Color::White);
    pauseMessage.setString("Welcome to the game of Pong!\nPress space to start the game");
}




#pragma mark 

///////
// Utility for handling startup resource problems
///////
void Pong::handleError(std::string s) {
    std::cout << s;
    exit(1);
}





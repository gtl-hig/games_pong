//
//  pong.h
//  pong_sfml
//
//  Created by Mariusz Nowostawski on 6/11/13.
//  Copyright (c) 2013 Mariusz Nowostawski. All rights reserved.
//

#ifndef pong_sfml_pong_h
#define pong_sfml_pong_h

#include <cmath>
#include <ctime>
#include <queue>
#include <cstdlib>

#include <SFML/Graphics.hpp>

#include "World.hpp"
#include "GameEvent.hpp"
#include "SimplePhysics.hpp"
#include "SimpleAI.hpp"



class Pong
{

public:
    
    // Create the world, and setup the rendering window
    Pong();
    // Clean up, release the rendering window
    ~Pong();
    

#pragma mark
    
    // the main game loop
    void run(void);


#pragma mark

private:

    void restartGame();
    
    void handleControllerEvents();
    void processGameEvents();
    
    void updateWorld();
    void renderWorld();
    
    void initializePauseMessage();
    void handleError(std::string s);

    
    
// member attributes
    
    bool isPlaying = false;
    
    sf::RenderWindow *window;
    
    World* world;
    SimpleAI* ai;
    SimplePhysics physics;
    
    std::queue<GameEvent*>* eventQueue;
    
    sf::Font font;
    sf::Text pauseMessage;
    sf::Clock clock;
    float deltaTime;

    
// constants
    const int gameWidth = 800;
    const int gameHeight = 600;
    
};
    
#endif
